import 'package:flutter/material.dart';
import './answer_button.dart';

class BodyQuestions extends StatelessWidget {
    //Para mashare yung data pero di hindi dedelete yung variable sa main
    //Lifting state app
    final questions;
    final int questionIdx;
    //if we are receiving a fucntion need lagyaan ng
    final Function nextQuestion;

    BodyQuestions({
        required this.questions,
        required this.questionIdx,
        required this.nextQuestion
    });
    

    @override
    Widget build(BuildContext context) {  
            Text questionText =  Text(
            //Para maacces yung question key
            questions[questionIdx]['question'].toString(),
                style: TextStyle(
                fontSize :24.0
            )
            
        );
        // Typecast oeprator 
        var options = questions[questionIdx]['options'] as List<String>;
        // Before Map['Time Tracking','Asset Management','Issue Tracking']
        // After Map [AnswerButton('Time Tracking'), AnsweButton('Asset Management), AnswerButton('Issue tracking')]
        var answerOptions = options.map((String option){
            return AnswerButton(text: option, nextQuestion: nextQuestion);
        });

        return Container(
        width: double.infinity,
        padding: EdgeInsets.all(16) ,
        child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                // mainAxisAlignment:  MainAxisAlignment.center,
                children: [
                    questionText,
                    //Para spread yung list
                    //[questionText, [answerOptions], skipQuestion]
                    //[questionText, answerButtonA, answerButtonB, answerButtonC, skipQuestion]
                    ...answerOptions,
                
                    Container(
                        margin: EdgeInsets.only(top:30),
                        width: double.infinity,
                        child: ElevatedButton(
                            child: Text('Skip Question'),
                            //execute later kaya waalang ()
                            onPressed: () => nextQuestion(null)
                        )
                    )
                ]
            ) 
        );   
    }
}
